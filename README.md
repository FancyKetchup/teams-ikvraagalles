# Ikvraagalles

Dit project is gemaakt als plugin voor Microsft Teams. Het moet inzicht verbeteren in voortgang van de studenten. Daarnaast kunnen docenten bijhouden hoe goed leerlingen opletten tijdens een online les.

Het project bestaat uit 3 delen: 
- API
- Bot
- Tab

## API
De API haalt data uit de database om het dashboard in Teams te kunnen weergeven. 

## Bot
De bot zorgt voor het afhandelen van nieuwe vragen die leerkrachten sturen, en het opslaan van antwoorden die leerlingen insturen. Deze worden vervolgens in een database opgeslagen.

## Tab
In de Tab wordt het dashboard weergegeven. Dit is een Tab die bovenaan verschijnt in een Teams channel. Hier kunnen leerkrachten kijken of leerlingen opletten en of de stof wordt begrepen.
