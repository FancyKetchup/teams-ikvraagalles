import React, { useEffect, useState } from "react"
import axios from "axios"
import { Card, CardHeader, Flex, Text, CardBody, Pill, Dropdown, Datepicker } from '@fluentui/react-northstar'
import chunk from 'lodash/chunk'
import "./Questions.css"


export function Questions ({ requestDate, onRequestDateChange }) {
  const [questions, setQuestions] = useState([])
  const [attribute, setAttribute] = useState('Correctheid')


  useEffect(() => {
    async function getData() {
      await getQuestionsOnDate(requestDate.date)
    }

    getData()
  }, [])

  async function getQuestionsOnDate(date) {
    date = date.toISOString().split('T')[0]
    const res = await axios.get(`http://localhost:3001/questions/${date}`)
    let questions = res.data.questions

    questions = chunk(questions, 4)

    setQuestions(questions)
  }


  const changeAttribute = async (value) => {
    setAttribute(value)
  }

  const changeDate = async (value) => {
    setAttribute('Correctheid')

    onRequestDateChange({ date: new Date(value) })
    getQuestionsOnDate(new Date(value))
  }

  const color = (percentage) => {
    if (percentage < 50) {
      return 'red'
    } else {
      return 'green'
    }
  }


  return <div>
    <Flex className="topcontrols" gap="gap.small">
      <Flex.Item size="size.half">
        <Dropdown
          items={['Correctheid', 'Invulpercentage']}
          value={attribute}
          placeholder="Attribuut"
          onChange={(e, { value }) => { changeAttribute(value) }}
        />
      </Flex.Item>
      <Flex.Item size="size.half">
        <Datepicker selectedDate={requestDate.date} onDateChange={(e, { value }) => { changeDate(value) }} allowManualInput={false} today={new Date()} />
      </Flex.Item>
    </Flex>
    {questions.length == 0 && <p>Er is geen data gevonden op de geselecteerde datum</p>}
    {questions.map(questionRule =>
      <Flex className="container" gap="gap.small">
        {questionRule.map(question =>
          <Card key={question.id} className="questioncard">
            <CardHeader>
              <Flex column>
                <Pill size="smaller">{question.sender}</Pill>
                <Text content={question.question} size="large" weight="bold" className="question-title" />
                { attribute == 'Correctheid' &&
                <Flex gap="gap.small">
                  <Flex.Item size="size.half">
                    <Text className="question-text" content={question.answer} />
                  </Flex.Item>
                  <Flex.Item size="size.half">
                    <Text className="question-text" color="green" content={Math.round(question.correctPercentage) + "%"} align="end" />
                  </Flex.Item>
                </Flex> }
                { attribute == 'Invulpercentage' && <Text className="question-text"  className={color(question.questionCoverage)} content={Math.round(question.questionCoverage) + "% heeft geantwoord"} /> }
                { attribute == 'Correctheid' &&
                <Flex gap="gap.small">
                  <Flex.Item size="size.half">
                    <Text className="question-text" content={question.other} />
                  </Flex.Item>
                  <Flex.Item size="size.half">
                    <Text className="question-text" color="red" content={Math.round(100 - question.correctPercentage) + "%"} align="end" />
                  </Flex.Item>
                </Flex>
}
              </Flex>
            </CardHeader>
            <CardBody>
            </CardBody>
          </Card>
        )}
      </Flex>
    )}
  </div>
}