import React, { useEffect, useState, useRef } from 'react'
import { Bar } from 'react-chartjs-2'
import axios from "axios"
import map from 'lodash/map'
import { Card, CardHeader, Flex, Text, CardBody, Pill, Dropdown, Datepicker } from '@fluentui/react-northstar'

import "./Summary.css"

export function Summary({ requestDate, onRequestDateChange }) {
  const [questionData, setQuestionData] = useState([])
  const [data, setData] = useState({})
  async function getQuestionsOnDate(date) {
    date = date.toISOString().split('T')[0]
    const res = await axios.get(`http://localhost:3001/questions/${date}`)
    let questions = res.data.questions

    setQuestionData(questions)
  }

  useEffect(() => {
    getQuestionsOnDate(requestDate.date)
  }, [])

  useEffect(() => {
    async function getData() {
      const labels = map(questionData, 'question')
      const questionCoverage = map(questionData, 'questionCoverage')
      const correctPercentage = map(questionData, 'correctPercentage')

      await setData({
        labels: labels,
        datasets: [
          {
            label: 'Invulpercentage',
            data: questionCoverage,
            backgroundColor: 'rgba(255, 206, 86, 0.2)',
            borderColor: 'rgba(255, 206, 86, 1)',
            borderWidth: 2,
          },
          {
            label: 'Correctheid',
            data: correctPercentage,
            backgroundColor: 'rgba(75, 192, 192, 0.2)',
            borderColor: 'rgba(75, 192, 192, 1)',
            borderWidth: 2,
          },
        ],
      })

    }

    getData()
  }, [questionData])

  const options = {
    indexAxis: 'x',
    // Elements options apply to all of the options unless overridden in a dataset
    // In this case, we are setting the border of each horizontal bar to be 2px wide
    elements: {
      bar: {
        borderWidth: 2,
      },
    },
    responsive: true,
    plugins: {
      legend: {
        position: 'right',
      },
      title: {
        display: true,
        text: 'Invulpercentage & Correctheid',
      },
    },
  }

  const changeDate = async (value) => {
    onRequestDateChange({ date: new Date(value) })
    getQuestionsOnDate(new Date(value))
  }

  return <div>
    <Flex className="topcontrols" gap="gap.small">
      <Flex.Item size="size.half">
        <div></div>
      </Flex.Item>
      <Flex.Item size="size.half">
        <Datepicker selectedDate={requestDate.date} onDateChange={(e, { value }) => { changeDate(value) }} allowManualInput={false} today={new Date()} />
      </Flex.Item>
    </Flex><Bar data={data} options={options} />
  </div>
}

