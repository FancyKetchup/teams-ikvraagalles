import React, { useEffect, useState } from "react"
import axios from "axios"
import { Avatar, Card, Flex, Text, Dropdown, Datepicker, Button } from '@fluentui/react-northstar'
import "./Users.css"

const orderBy = require("lodash/orderBy")

export function Users({ requestDate, onRequestDateChange }) {
  const [users, setUsers] = useState([])
  const [attribute, setAttribute] = useState('Correctheid')

  useEffect(() => {
    async function getData() {
      await getUsersOnDate(requestDate.date)
    }

    getData()

  }, [])

  async function getUsersOnDate(date) {
    date = date.toISOString().split('T')[0]
    const res = await axios.get(`http://localhost:3001/students/${date}`)

    const orderedByCorrect = orderBy(res.data.users, ['correctPercentage'], ['asc'])

    setUsers(orderedByCorrect)
  }

  const color = (percentage) => {
    if (percentage < 50) {
      return 'red'
    } else {
      return 'green'
    }
  }

  const changeAttribute = async (value) => {
    setAttribute(value)
    if (value == 'Correctheid') {
      const orderedByCorrect = orderBy(users, ['correctPercentage'], ['asc'])
      setUsers(orderedByCorrect)
    } else {
      const orderedByCorrect = orderBy(users, ['questionCoverage'], ['asc'])
      setUsers(orderedByCorrect)
    }
  }

  const changeDate = async (value) => {
    setAttribute('Correctheid')

    onRequestDateChange({ date: new Date(value) })
    getUsersOnDate(new Date(value))
  }

  const wakeUp = async () => {
    console.log("hoi")
    await axios.post("http://localhost:3001/wake")
  }

  return <div>
    <Flex className="topcontrols" gap="gap.small">
      <Flex.Item size="size.half">
        <Dropdown
          items={['Correctheid', 'Invulpercentage']}
          value={attribute}
          placeholder="Attribuut"
          onChange={(e, { value }) => { changeAttribute(value) }}
        />
      </Flex.Item>
      <Flex.Item size="size.half">
        <Datepicker selectedDate={requestDate.date} onDateChange={(e, { value }) => { changeDate(value) }} allowManualInput={false} today={new Date()} />
      </Flex.Item>
    </Flex>
    <Flex gap="gap.small">
      {users.length == 0 && <p>Er is geen data gevonden op de geselecteerde datum</p>}
      {users.map((user) =>
        <Card size="small" aria-roledescription="card with avatar, image and text">
          <Card.Header>
            <Flex gap="gap.small">
              <Avatar
                image="https://fabricweb.azureedge.net/fabric-website/assets/images/avatar/RobertTolbert.jpg"
              />
              <Flex column>
                <Text content={user.user} weight="bold" />
                <Text content="Student" size="small" />
              </Flex>
            </Flex>
          </Card.Header>
          <Card.Body>
            <Flex column gap="gap.small">
              {attribute == 'Correctheid' && <Text className={color(user.correctPercentage)} content={Math.round(user.correctPercentage) + "% correct"} />}
              {attribute == 'Invulpercentage' && <Text className={color(user.questionCoverage)} content={Math.round(user.questionCoverage) + "% ingevuld"} />}
              <Button content="Wake up" size="small" onClick={wakeUp} />
            </Flex>
          </Card.Body>
        </Card>
      )}
    </Flex>
  </div>
}