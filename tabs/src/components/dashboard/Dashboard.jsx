import React, { useEffect, useState } from "react"
import axios from "axios"
import { Menu } from '@fluentui/react-northstar'
import { Users } from "./Users"
import { Questions } from "./Questions"
import { Summary } from "./Summary"

import "./Dashboard.css"


export function Dashboard() {
  const [selectedMenuItem, setSelectedMenuItem] = useState("users")
  const [requestDate, setRequestDate] = useState({ date: new Date() })

  const menuKeys = ["users", "questions", "summary"]
  const menuItemsNames = {
    users: "Gebruikers",
    questions: "Vragen",
    summary: "Samenvatting"
  }

  const menuItems = menuKeys.map((key) => {
    return {
      key,
      content: menuItemsNames[key],
      onClick: () => setSelectedMenuItem(key)
    }
  })

  const handeRequestDateChange = (date) => {
    setRequestDate(date)
  }

  return (
    <div>
      <Menu defaultActiveIndex={0} items={menuItems} />
      <div class="menu-content">
        {selectedMenuItem === "users" && (
          <Users requestDate={requestDate} onRequestDateChange={handeRequestDateChange} />
        )}
        {selectedMenuItem === "questions" && (
          <Questions requestDate={requestDate} onRequestDateChange={handeRequestDateChange} />
        )}
        {selectedMenuItem === "summary" && (
          <Summary requestDate={requestDate} onRequestDateChange={handeRequestDateChange} />
        )}
      </div>
    </div>
  )
}