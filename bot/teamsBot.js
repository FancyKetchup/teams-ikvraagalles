const {
  TeamsActivityHandler,
  tokenExchangeOperationName,
  ActionTypes,
  CardFactory,
  TeamsInfo
} = require("botbuilder")
const ACdata = require("adaptivecards-templating")
const shuffle = require("lodash/shuffle")
const axios = require("axios")
const querystring = require("querystring")
const mysql = require('mysql')
const util = require("util")

const conn = mysql.createConnection({
  host: "localhost",
  user: "patrick",
  password: "patrick",
  database: "ikvraagalles",
  port: 13306
})

conn.query = util.promisify(conn.query).bind(conn)

class TeamsBot extends TeamsActivityHandler {

  correctAnswer = ''

  /**
   *
   * @param {ConversationState} conversationState
   * @param {UserState} userState
   * @param {Dialog} dialog
   */
  constructor(conversationState, userState, dialog) {
    super()
    if (!conversationState) {
      throw new Error("[TeamsBot]: Missing parameter. conversationState is required")
    }
    if (!userState) {
      throw new Error("[TeamsBot]: Missing parameter. userState is required")
    }
    if (!dialog) {
      throw new Error("[TeamsBot]: Missing parameter. dialog is required")
    }
    this.conversationState = conversationState
    this.userState = userState
    this.dialog = dialog
    this.dialogState = this.conversationState.createProperty("DialogState")

    this.onMessage(async (context, next) => {
      if (context.activity.value) {
        await this.handleAnswer(context)
      } else {
        await this.dialog.run(context, this.dialogState)
      }

      // By calling next() you ensure that the next BotHandler is run.
      await next()
    })

    this.onMembersAdded(async (context, next) => {
      const membersAdded = context.activity.membersAdded
      for (let cnt = 0; cnt < membersAdded.length; cnt++) {
        if (membersAdded[cnt].id) {
          const cardButtons = [
            { type: ActionTypes.ImBack, title: "Show introduction card", value: "intro" },
          ]
          const card = CardFactory.heroCard("Welcome", null, cardButtons, {
            text: `Congratulations! Your hello world Bot 
                            template is running. This bot has default commands to help you modify it.
                            You can reply <strong>intro</strong> to see the introduction card. This bot is built with <a href=\"https://dev.botframework.com/\">Microsoft Bot Framework</a>`,
          })
          await context.sendActivity({ attachments: [card] })
          break
        }
      }
      await next()
    })
  }

  async run(context) {
    await super.run(context)

    // Save any state changes. The load happened during the execution of the Dialog.
    await this.conversationState.saveChanges(context, false)
    await this.userState.saveChanges(context, false)
  }

  async handleTeamsSigninVerifyState(context, query) {
    console.log("Running dialog with signin/verifystate from an Invoke Activity.")
    await this.dialog.run(context, this.dialogState)
  }

  async handleTeamsSigninTokenExchange(context, query) {
    await this.dialog.run(context, this.dialogState)
  }

  async onTokenResponseEvent(context) {
    console.log("Running dialog with Token Response Event Activity.")

    // Run the Dialog with the new Token Response Event Activity.
    await this.dialog.run(context, this.dialogState)
  }

  async onSignInInvoke(context) {
    if (context.activity && context.activity.name === tokenExchangeOperationName) {
      if (await this.dialog.shouldDedup(context)) {
        return
      }
    }
    await this.dialog.run(context, this.dialogState)
  }

  handleTeamsMessagingExtensionSubmitAction(context, action) {
    switch (action.commandId) {
      case "createCard":
        return this.createQuestion(context, action)
      default:
        throw new Error("NotImplemented")
    }
  }

  async createQuestion(context, action) {
    const data = action.data
    let options = [{ title: data.answer, value: data.answer }, { title: data.other, value: data.other }]
    options = shuffle(options)

    this.correctAnswer = data.answer

    const participants = await (await TeamsInfo.getTeamDetails(context)).memberCount

    const result = await conn.query("INSERT INTO question (date, question, answer, other, sender, participants) VALUES (?, ?, ?, ?, ?, ?)", [new Date(), data.question, data.answer, data.other, context.activity.from.name, participants])
    const insertId = result.insertId

    const adaptiveCardSource = require("../poll.json")
    const template = new ACdata.Template(adaptiveCardSource)


    const filledCard = template.expand({
      $root: {
        title: data.question,
        id: insertId,
        options: options
      }
    })

    const card = CardFactory.adaptiveCard(filledCard)

    const responseActivity = {
      type: 'message', attachments: [card], channelData: {
        onBehalfOf: [
          { itemId: 0, mentionType: 'person', mri: context.activity.from.id, displayname: context.activity.from.name }
        ]
      }
    }


    await context.sendActivity(responseActivity)
  }

  async handleAnswer(context) {
    const activity = context.activity
    // const members = await TeamsInfo.getMembers(context)
    const user = activity.from.name
    const questionId = activity.value.questionId

    let correctMessage = {
      type: 'message',
      onBehalfOf: [
        { itemId: 0, mentionType: 'person', mri: context.activity.from.id, displayname: context.activity.from.name }
      ]
    }

    const question = await conn.query("SELECT * FROM question WHERE id = ?", [questionId])

    if (activity.value.answer == question[0].answer) {
      correctMessage.text = user + " heeft het goed"
    } else {
      correctMessage.text = user + " heeft het verkeerd"
    }

    //Prevent double answers
    const answerGiven = await conn.query("SELECT * FROM answer WHERE question_id = ? AND user = ?", [questionId, user])

    if(answerGiven.length > 0) {
      return false
    }

    await conn.query("INSERT INTO answer (date, question_id, user, answer, correct) VALUES (?, ?, ?, ?, ?)", [new Date(), questionId, user, activity.value.answer, (activity.value.answer == question[0].answer)])

    await context.sendActivity(correctMessage)

  }
}

module.exports.TeamsBot = TeamsBot
