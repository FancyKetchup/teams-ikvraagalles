const express = require('express')
const app = express()
const port = 3001
const cors = require('cors')
const mysql = require('mysql')
const util = require("util")
const axios = require('axios');

const conn = mysql.createConnection({
  host: "localhost",
  user: "patrick",
  password: "patrick",
  database: "ikvraagalles",
  port: 13306
})

conn.query = util.promisify(conn.query).bind(conn)

app.use(cors())

app.post('/wake', async (req, res) => {
  await axios.post("https://fhictsky.webhook.office.com/webhookb2/cbaf2b24-8348-4784-92e5-1a327e56d88a@0172c9f5-f568-42ac-9eb8-24ef84881faa/IncomingWebhook/1ccb9b3c2d3c4d2a898de91957c413a5/be103434-2d6f-4772-bf2b-d7633d3c67cd", {
    "type": "AdaptiveCard",
    "text": "<strong>IJntema,Patrick P.</strong> Opletten!",
    "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
    "version": "1.3"
  })
})


app.get('/students/:date', async (req, res) => {
  const result = await conn.query("SELECT user, SUM(correct) / COUNT(correct) * 100 as correctPercentage, COUNT(*)/(SELECT COUNT(*) FROM question WHERE question.date = ?) * 100 as questionCoverage FROM answer WHERE answer.date = ? GROUP BY user", [req.params.date, req.params.date])

  res.json({ users: result })
})

app.get('/questions/:date', async (req, res) => {
  const result = await conn.query("SELECT answer.question_id, question.question, question.date, question.answer, question.other, question.sender, SUM(answer.correct)/COUNT(answer.correct) * 100 as correctPercentage, COUNT(answer.id)/question.participants * 100 as questionCoverage FROM `question` LEFT JOIN answer ON question.id = answer.question_id WHERE question.date = ? GROUP BY answer.question_id", [req.params.date])

  res.json({ questions: result })
})


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})